import tensorflow as tf
import numpy as np
import argparse
import json

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--config',
                    default='config.json',
                    dest='config',
                    help='path to config.json with all variables')
args = parser.parse_args()
CONFIG_FILE= args.config
with open(CONFIG_FILE, 'r') as f:
    json_config = json.load(f)
MODEL_PATH = json_config['facenet_model_path_graph']
FINAL_MODEL_PATH = json_config['decision_model_path']
LABELS_PATH = json_config['labels_path']
SEED = json_config['seed']
IMAGE_SIZE = json_config['image_size']
BATCH_SIZE = json_config['batch_size']
DATA_DIR = json_config['imgs']
EXPORT_PATH = './models/protobuf/1'

if __name__=='__main__':
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(MODEL_PATH, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
        #with tf.Graph().as_default():
        session_conf = tf.ConfigProto(
                            device_count={'GPU': 0},
                            allow_soft_placement=True,
                            log_device_placement=False)
        with tf.Session(config=session_conf) as sess:
            np.random.seed(seed=SEED)
            #init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
            #facenet.load_model(MODEL_PATH)
            # init_op = tf.group(tf.global_variables_initializer())
            # sess.run(init_op)
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
            #create info protos
            images_placeholder_info = tf.saved_model.utils.build_tensor_info(images_placeholder)
            embeddings_info = tf.saved_model.utils.build_tensor_info(embeddings)
            phase_info = tf.saved_model.utils.build_tensor_info(phase_train_placeholder)
            ##exorting to protobuf
            builder = tf.saved_model.builder.SavedModelBuilder(EXPORT_PATH)
            prediction_signature = (
                                tf.saved_model.signature_def_utils.build_signature_def( 
                                    inputs={'images': images_placeholder_info,
                                            'phase': phase_info},
                                    outputs={'outputs': embeddings_info},
                                    method_name=\
                                        tf.saved_model.signature_constants.PREDICT_METHOD_NAME)
                                   )
            legacy_init_op = tf.group(tf.tables_initializer(), 
                                     name='legacy_init_op') 
            print ("HERE IM OK")
            legacy_init_op = tf.group(tf.tables_initializer(), name='legacy_init_op')
            builder.add_meta_graph_and_variables(
                                            sess, 
                                            [tf.saved_model.tag_constants.SERVING], 
                                            signature_def_map={'predict_images': prediction_signature},
                                            legacy_init_op=legacy_init_op)
            builder.save()
